package com.example.loginregisterproject;

public class User {
    public String username;
    public String email;
    public String phoneNo;
    public String birthday;

    public User() {
    }

    public User(String name, String email, String phone, String birthday) {
        this.username = name;
        this.email = email;
        this.phoneNo = phone;
        this.birthday = birthday;
    }

    public String getUsername() {
        return username;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }
}
