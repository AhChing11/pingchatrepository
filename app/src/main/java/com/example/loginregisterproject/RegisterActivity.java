package com.example.loginregisterproject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {

    TextView emailInput, passwordInput, confirmPasswordInput, loginDirect;
    Button signUpButton;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mAuth = FirebaseAuth.getInstance();
        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);
        signUpButton = findViewById(R.id.signUpButton);
        confirmPasswordInput = findViewById(R.id.confirmPasswordInput);
        loginDirect = findViewById(R.id.loginDirect);

        if (getIntent().hasExtra("email") && getIntent().hasExtra("password")) {
            String email = getIntent().getStringExtra("email");
            String password = getIntent().getStringExtra("password");

            emailInput.setText(email);
            passwordInput.setText(password);
            confirmPasswordInput.setText(password);
        }


        loginDirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, Login.class));
            }
        });


        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View V) {
                String email = emailInput.getText().toString();
                String password = passwordInput.getText().toString();
                String confirmPassword = confirmPasswordInput.getText().toString();

                if (email.isEmpty() && password.isEmpty() && confirmPassword.isEmpty()) {
                    emailInput.setError("Please Enter Email Address.");
                    passwordInput.setError("Please Enter Password");
                    confirmPasswordInput.setError("Please Re-enter Password");
                } else if (email.isEmpty() && password.isEmpty()) {
                    emailInput.setError("Please Enter Email Address.");
                    passwordInput.setError("Please Enter Password");
                } else if (password.isEmpty() && confirmPassword.isEmpty()) {
                    passwordInput.setError("Please Enter Password");
                    confirmPasswordInput.setError("Please Re-enter Password");
                } else if (email.isEmpty() && confirmPassword.isEmpty()) {
                    emailInput.setError("Please Enter Email Address.");
                    confirmPasswordInput.setError("Please Re-enter Password");
                } else if (email.isEmpty()) {
                    emailInput.setError("Please Enter Email Address.");
                } else if (password.isEmpty()) {
                    passwordInput.setError("Please Enter Password");
                } else if (confirmPassword.isEmpty()) {
                    confirmPasswordInput.setError("Please Re-enter Password");
                } else if (!(email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty())) {
                    if (password.equals(confirmPassword)) {
                        Intent intent = new Intent(RegisterActivity.this, ProfileSetup.class);
                        intent.putExtra("email", email);
                        intent.putExtra("password", password);
                        startActivity(intent);
                    } else {
                        confirmPasswordInput.setError("Your password doesn't match");
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, "Please check your details.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    //



}
