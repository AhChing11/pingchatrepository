package com.example.loginregisterproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.Button;

public class DonateSelection extends AppCompatActivity {
    static int position;
    Button backButton;
    static Button nextButton;

    String charity[], description[], more[];
    int images[]= {R.drawable.hands, R.drawable.story1, R.drawable.story2, R.drawable.story3, R.drawable.story4};
    boolean checkState[] = new boolean[20];
    RecyclerView charityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate_selection);

        backButton = findViewById(R.id.backButton);
        nextButton= findViewById(R.id.nextButton);
        charityList = findViewById(R.id.charityList);

        charity = getResources().getStringArray(R.array.charity);
        description = getResources().getStringArray(R.array.description);
        more = getResources().getStringArray(R.array.more);

        final View parent = (View) backButton.getParent();  // button: the view you want to enlarge hit area
        parent.post( new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                backButton.getHitRect(rect);
                rect.top -= 100;    // increase top hit area
                rect.left -= 100;   // increase left hit area
                rect.bottom += 100; // increase bottom hit area
                rect.right += 100;  // increase right hit area
                parent.setTouchDelegate( new TouchDelegate( rect , backButton));
            }
        });

        for(int i = 0; i<images.length;i++) {
            checkState[i] = false;
        }

        CharityListAdapter charityListAdapter = new CharityListAdapter(this, charity, description, images, checkState);
        charityList.setAdapter(charityListAdapter);
        charityList.setLayoutManager(new LinearLayoutManager(this));


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DonateSelection.this, MainActivity.class));
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DonateSelection.this, DonationAmount.class);
                intent.putExtra("charityData", charity[position]);
                intent.putExtra("charityImage", images[position]);
                startActivity(intent);
            }
        });
    }

    public static void enableNextButton() {
        nextButton.setEnabled(true);
        nextButton.setBackgroundResource(R.drawable.oval);
    }


    public static void setPosition(int position) {
        DonateSelection.position = position;
    }
}
