package com.example.loginregisterproject;

public class MoreItem {
    private int menuIcon;
    private String menuDescription;

    public MoreItem(String menuDescription, int menuIcon) {
        this.menuDescription = menuDescription;
        this.menuIcon = menuIcon;
    }

    public String getMenuDescription() {
        return this.menuDescription;
    }

    public int getMenuIcon() {
        return this.menuIcon;
    }
}
