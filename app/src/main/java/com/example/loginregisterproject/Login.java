package com.example.loginregisterproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class Login extends AppCompatActivity {
    public static TextView emailInput;
    TextView passwordInput;
    TextView forgetPassword;
    Button loginButton;
    Button signUpButton;
    FirebaseAuth mAuth;
    String email;
    String password;
    private SignInButton googleSignIn;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 9001;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);
        loginButton = findViewById((R.id.loginButton));
        signUpButton = findViewById(R.id.signUpButton);
        forgetPassword = findViewById(R.id.forgetPassword);
        googleSignIn = findViewById(R.id.googleSignIn);
        mAuth = FirebaseAuth.getInstance();

        final View parent = (View) forgetPassword.getParent();  // button: the view you want to enlarge hit area
        parent.post( new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                forgetPassword.getHitRect(rect);
                rect.top -= 30;    // increase top hit area
                rect.left -= 30;   // increase left hit area
                rect.bottom += 30; // increase bottom hit area
                rect.right += 30;  // increase right hit area
                parent.setTouchDelegate( new TouchDelegate( rect , forgetPassword));
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Login.this, "This function is still under maintenance.", Toast.LENGTH_SHORT).show();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void  onClick(View V) {
                startActivity(new Intent(Login.this, RegisterActivity.class));
                finish();
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mFirebaseUser = mAuth.getCurrentUser();
                if(mFirebaseUser != null) {
                    System.out.println("-----------------Account Exists");
                }


            }
        };

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailInput.getText().toString();
                password = passwordInput.getText().toString();

                if(email.isEmpty()) {
                    emailInput.setError("Please Enter Email Address.");
                }
                else if(password.isEmpty()) {
                    passwordInput.setError("Please Enter Password");
                }
                else if(email.isEmpty() && password.isEmpty()) {
                    emailInput.setError("Please Enter Email Address.");
                    passwordInput.setError("Please Enter Password");
                }
                else if(!(email.isEmpty() && password.isEmpty())) {

                    mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                                startActivity(new Intent(Login.this, MainActivity.class));
                        }
                    });
                }
                else {
                    Toast.makeText(Login.this, "Error Logging In.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode("61142311653-v00soa3926dg5ko9dshcr0pem5mtut55.apps.googleusercontent.com")
                .requestIdToken("61142311653-v00soa3926dg5ko9dshcr0pem5mtut55.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //
        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                FirebaseGoogleAuth(account);
                System.out.println("------------------------------signed in Credential -> RegisterActivity");
            } catch (Exception ex) {
                System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                ex.printStackTrace();
                Toast.makeText(Login.this, "Registration Error2", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void FirebaseGoogleAuth(GoogleSignInAccount acct) {
        AuthCredential authCredential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        System.out.println("------------------------> FirebaseGoogleAuth() -> RegisterActivity");
        mAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    System.out.println("-------------------------checkUserData() -> RegisterActivity");
                    checkUserData(mAuth.getCurrentUser().getUid());
                } else {
                    Toast.makeText(Login.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void checkUserData(final String userId){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                System.out.println("----------------------------------checkUserData() -> Login");
                //if there is any empty child
                if(!(dataSnapshot.hasChild("birthday") && dataSnapshot.hasChild("profilePicUrl") && dataSnapshot.hasChild("phoneNo") && dataSnapshot.hasChild("username"))) {
                    Intent intent = new Intent(Login.this, ProfileSetup.class);
                    intent.putExtra("uid", userId);

                    mAuth.signOut();
                    System.out.println("-------------------bring User to ProfileSetup -> RegisterActivity");
                    startActivity(intent);
                }
                //If every child is present
                else {
                    startActivity(new Intent(Login.this, MainActivity.class));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
