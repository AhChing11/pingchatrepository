package com.example.loginregisterproject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.net.URI;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileSetup extends AppCompatActivity {
    CircleImageView profilePicture;
    EditText usernameInput;
    EditText phoneInput;
    EditText birthdayInput;
    Button backButton;
    Button saveButton;
    FirebaseAuth mAuth;
    String email, password;
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Users");
    StorageReference storageReference = FirebaseStorage.getInstance().getReference("ProfilePicture");

    private Uri mImageUri;
    private StorageTask storageTask;
    String myUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setup);

        profilePicture = findViewById(R.id.profilePicture);
        usernameInput = findViewById(R.id.usernameInput);
        phoneInput = findViewById(R.id.phoneInput);
        birthdayInput = findViewById(R.id.birthdayInput);
        backButton = findViewById(R.id.backButton);
        saveButton = findViewById(R.id.saveButton);
        mAuth = FirebaseAuth.getInstance();

        //if intent is from RegisterActivity
        retrieveIntentData();

        final GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            String username = acct.getEmail().split("@")[0];
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            System.out.println(personId + "+++++++++++++++++++ProfileSetup");
            Uri personPhoto = acct.getPhotoUrl();

            usernameInput.setText(username);
            Glide.with(this).load(String.valueOf(personPhoto)).into(profilePicture);
            mImageUri = Uri.parse(acct.getPhotoUrl().toString());
        }

        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity().setAspectRatio(9, 16).start(ProfileSetup.this);
            }
        });

        //put email & password retrieve from RegisterActivity back to RegisterActivity if back button is clicked
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileSetup.this, RegisterActivity.class);

                intent.putExtra("email", email);
                intent.putExtra("password", password);

                startActivity(intent);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = usernameInput.getText().toString();
                final String phoneNo = phoneInput.getText().toString();
                final String birthday = birthdayInput.getText().toString();

                if (mImageUri == null) {
                    Toast.makeText(ProfileSetup.this, "Please insert a Profile Picture", Toast.LENGTH_SHORT).show();
                } else if (phoneNo.isEmpty()) {
                    phoneInput.setError("Please Enter Phone Number");
                } else if (birthday.isEmpty()) {
                    birthdayInput.setError("Please Enter Your Birthday");
                } else if (username.isEmpty()) {
                    usernameInput.setError("Please Enter Username");
                } else if (!(username.isEmpty() && phoneNo.isEmpty() && birthday.isEmpty() && mImageUri == null) && getIntent().hasExtra("uid")) {
                    System.out.println("---------------------hasuid -> ProfileSetup");
                    mDatabase = mDatabase.child(getIntent().getStringExtra("uid"));
                    mDatabase.child("username").setValue(username.trim());
                    mDatabase.child("phoneNo").setValue(phoneNo.trim());
                    mDatabase.child("birthday").setValue(birthday.trim());

                    //if not the default image from Google
                    if (mImageUri.toString() != acct.getPhotoUrl().toString()) {
                        System.out.println("----------------------newImageSelected -> ProfileSetup");
                        final StorageReference imageReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(mImageUri));
                        storageTask = imageReference.putFile(mImageUri);
                        storageTask.continueWithTask(new Continuation() {
                            @Override
                            public Task<Uri> then(@NonNull Task task) throws Exception {
                                if (!task.isSuccessful()) {
                                    System.out.println("-------------------------------fail");
                                    throw task.getException();
                                }
                                System.out.println("-------------------------------continue");
                                return imageReference.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    mImageUri = task.getResult();
                                }
                            }
                        });
                    }
                    System.out.println("----------------------useDefaultImage -> ProfileSetup");
                    mDatabase.child("profilePicUrl").setValue(mImageUri.toString());


                    AuthCredential authCredential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
                    mAuth.signInWithCredential(authCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            startActivity(new Intent(ProfileSetup.this, MainActivity.class));
                            finish();
                        }
                    });


//                else if (!(username.isEmpty() && phone.isEmpty() && birthday.isEmpty() && mImageUri.equals(null)))
                } else {
                    System.out.println("---------------------newuser -> ProfileSetup");
                    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(ProfileSetup.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            mAuth.signOut();
                            if (!task.isSuccessful()) {
                                System.out.println("---------------Fail registration");
                                Log.d("Register Error", "Error");
                                Toast.makeText(ProfileSetup.this, "SignUp Unsuccessful.", Toast.LENGTH_SHORT).show();
                            } else {
                                System.out.println("---------------succesful registration");
                                createNewUser(task.getResult().getUser(), username, email, phoneNo, birthday);
                                uploadProfilePhoto(task.getResult().getUser());
//                                Toast.makeText(ProfileSetup.this, "Account created successfully.", Toast.LENGTH_SHORT).show();
//                                mAuth.signInWithEmailAndPassword(email, password);
//                                startActivity(new Intent(ProfileSetup.this, MainActivity.class));
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("-----------------onActivityResult() -> ProfileSetup");
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            mImageUri = result.getUri();
            profilePicture.setImageURI(mImageUri);
        } else {
            Intent intent = new Intent(ProfileSetup.this, ProfileSetup.class);
            final String username = usernameInput.getText().toString();
            final String phoneNo = phoneInput.getText().toString();
            final String birthday = birthdayInput.getText().toString();

            intent.putExtra("email", email);
            intent.putExtra("password", password);
            intent.putExtra("username", username);
            intent.putExtra("phoneNo", phoneNo);
            intent.putExtra("birthday", birthday);

            startActivity(intent);
            finish();
        }
    }

    public void createNewUser(FirebaseUser user, String username, String email, String phone, String birthday) {
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        User newUser = new User(username, email, phone, birthday);
        mDatabase.child("Users").child(user.getUid()).setValue(newUser);
        System.out.println("--------------------->createdNewUSer into Database");

    }

    // set text from previously filled data;
    public void retrieveIntentData() {
        if(getIntent().hasExtra("email")) {
            email = getIntent().getStringExtra("email");
        }
        if(getIntent().hasExtra("password")) {
            password = getIntent().getStringExtra("password");
        }
        if (getIntent().hasExtra("birthday")) {
            birthdayInput.setText(getIntent().getStringExtra("birthday"));
        }
        if (getIntent().hasExtra("phoneNo")) {
            phoneInput.setText(getIntent().getStringExtra("phoneNo"));
        }
        if (getIntent().hasExtra("username")) {
            usernameInput.setText(getIntent().getStringExtra("username"));
        }
        if (getIntent().hasExtra("profilePicUrl")) {
            Picasso.get().load(getIntent().getStringExtra("profilePicUrl")).into(profilePicture);
//            String uri = getIntent().getStringExtra("profilePicUrl");
//            mImageUri = Uri.parse(uri);
        }

    }

    private void uploadProfilePhoto(final FirebaseUser user) {
        if (mImageUri != null) {
            final StorageReference imageReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(mImageUri));
            storageTask = imageReference.putFile(mImageUri);
            storageTask.continueWithTask(new Continuation() {
                @Override
                public Task<Uri> then(@NonNull Task task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return imageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        myUrl = downloadUri.toString();

                        String userid = user.getUid();

                        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
                        reference.child("profilePicUrl").setValue(myUrl);

                        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                startActivity(new Intent(ProfileSetup.this, MainActivity.class));
                                finish();
                            }
                        });

                        Toast.makeText(ProfileSetup.this, "Account created successfully.", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ProfileSetup.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(ProfileSetup.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "No image selected", Toast.LENGTH_SHORT).show();
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }



}


