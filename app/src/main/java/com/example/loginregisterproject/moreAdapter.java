package com.example.loginregisterproject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class moreAdapter extends RecyclerView.Adapter<moreAdapter.MoreViewHolder> {
    Context context;

    private ArrayList<MoreItem> moreList;

    public moreAdapter(Context context, ArrayList<MoreItem> moreList) {
        this.context = context;
        this.moreList= moreList;
    }

    @NonNull
    @Override
    public MoreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_item, parent, false);

         MoreViewHolder moreViewHolder = new MoreViewHolder(v);
         return moreViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MoreViewHolder holder,final int position) {
        MoreItem currentItem = moreList.get(position);

        holder.moreIcon.setImageResource(currentItem.getMenuIcon());
        holder.moreDescription.setText(currentItem.getMenuDescription());

        holder.moreItem.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(position == 0) {
                    context.startActivity(new Intent(context, DonateSelection.class));
                } else if(position == 1) {
                    context.startActivity(new Intent(context, createCharity.class));
                } else if(position == 2) {
                    context.startActivity(new Intent(context, MoreUpdate.class));
                } else if(position == 3) {
                    context.startActivity(new Intent(context, Notification.class));
                } else {
                    context.startActivity(new Intent(context, MoreFragment.class));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return moreList.size();
    }



    public static class MoreViewHolder extends RecyclerView.ViewHolder {
        public ImageView moreIcon;
        public TextView moreDescription;
        public CardView moreItem;

        public MoreViewHolder(@NonNull View itemView) {
            super(itemView);

            moreIcon = itemView.findViewById(R.id.moreIcon);
            moreDescription = itemView.findViewById(R.id.moreDescription);
            moreItem = itemView.findViewById(R.id.moreItem);
        }
    }
}
