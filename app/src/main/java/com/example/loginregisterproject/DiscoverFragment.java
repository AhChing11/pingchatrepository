package com.example.loginregisterproject;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DiscoverFragment extends Fragment {
    RecyclerView storyView;
    StoryListAdapter storyListAdapter;
    List<Story> storyList;
    private BottomSheetBehavior mBottomSheetBehavior;
    RecyclerView moreList;
    CoordinatorLayout discoverFragment;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discover, container, false);
        View bottom_sheet = view.findViewById(R.id.bottom_sheet);
        RelativeLayout bottomSheet = view.findViewById(R.id.bottomSheet);
        discoverFragment = view.findViewById(R.id.discoverFragment);


        storyView = view.findViewById(R.id.storyView);
        storyView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        storyView.setLayoutManager(layoutManager);

        storyList = new ArrayList<>();
        storyListAdapter = new StoryListAdapter(getContext(), storyList);
        storyView.setAdapter(storyListAdapter);


        moreList = view.findViewById(R.id.moreList);
        setMoreList(moreList);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        mBottomSheetBehavior.setPeekHeight(200);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setPeekHeight(200);
                }
            }

            @Override
            public void onSlide(View bottomSheet, final float slideOffset) {

            }
        });

        bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });


//        <----------------- CLICK OUTSIDE TO CLOSE TAB -------------------->
//
//        discoverFragment.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//            }
//        });


        readStory();
        return view;
    }

    private void readStory() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Story");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                storyList.clear();
                System.out.println("-------------------readStory() -> DiscoverFragment");
                System.out.println(FirebaseAuth.getInstance().getCurrentUser().getUid());
                storyList.add(new Story("", 0, 0, "", FirebaseAuth.getInstance().getCurrentUser().getUid(), ""));

                storyListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setMoreList(RecyclerView moreViewList) {
        RecyclerView.Adapter moreAdapter;

        ArrayList<MoreItem> moreArrayList = new ArrayList<>();
        moreArrayList.add(new MoreItem("Donate", R.drawable.donation_icon1));
        moreArrayList.add(new MoreItem("Add Charity", R.drawable.add_charity_icon));
        moreArrayList.add(new MoreItem("Update Log", R.drawable.update_log_icon));
        moreArrayList.add(new MoreItem("Notification", R.drawable.notification_icon));

        moreViewList.setHasFixedSize(true);

        moreAdapter = new moreAdapter(getContext(), moreArrayList);

        moreViewList.setLayoutManager(new GridLayoutManager(getContext(), 3));
        moreViewList.setAdapter(moreAdapter);
    }
}
