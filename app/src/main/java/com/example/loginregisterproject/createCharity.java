package com.example.loginregisterproject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.TouchDelegate;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.collect.BiMap;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

public class createCharity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 1;
    Button uploadButton;
    Button createCharityButton;
    Button backButton;
    TextView charityTitle;
    TextView fileName;
    TextView charityDescription;
    Uri charityImage;
    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;

    String displayName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_charity);

        uploadButton = findViewById(R.id.uploadButton);
        charityTitle = findViewById(R.id.charityTitle);
        fileName = findViewById(R.id.fileName);
        charityDescription = findViewById(R.id.charityDescription);
        createCharityButton = findViewById(R.id.createCharityButton);
        backButton = findViewById(R.id.backButton);

        mStorageRef = FirebaseStorage.getInstance().getReference("charityUploads");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Charity");

        final View parent = (View) backButton.getParent();  // button: the view you want to enlarge hit area
        parent.post(new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                backButton.getHitRect(rect);
                rect.top -= 100;    // increase top hit area
                rect.left -= 100;   // increase left hit area
                rect.bottom += 100; // increase bottom hit area
                rect.right += 100;  // increase right hit area
                parent.setTouchDelegate(new TouchDelegate(rect, backButton));
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        createCharityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = charityTitle.getText().toString();
                String description = charityDescription.getText().toString();

                String charityId = createNewCharity(title, description);
                uploadFile(charityId);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(createCharity.this, MainActivity.class));
            }
        });
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            charityImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), charityImage);

                String imageName[] = {MediaStore.Images.Media.DISPLAY_NAME};

                Cursor cursor = getContentResolver().query(data.getData(), imageName, null, null, null);

                if (cursor == null) {
                    Toast.makeText(this, "cursor==null\n\ncould not query content resolver for", Toast.LENGTH_SHORT).show();
                    return;
                }
                cursor.moveToFirst();
                displayName = cursor.getString(0);

                fileName.setText(displayName);
                cursor.close();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile(final String charityId) {

        if (charityImage != null) {
            StorageReference fileReference = mStorageRef.child(System.currentTimeMillis() + "." + getFileExtension(charityImage));

            fileReference.putFile(charityImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(createCharity.this, "Upload successful", Toast.LENGTH_LONG).show();

                            Upload upload = new Upload(displayName, taskSnapshot.getStorage().getDownloadUrl().toString());

                            String uploadId = mDatabaseRef.push().getKey();
//                            mDatabaseRef.child("charityID").child("image").child(uploadId).setValue(upload);
                            mDatabaseRef.child(charityId).child("image").setValue(upload);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(createCharity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        } else {
            Toast.makeText(this, "No file selected.", Toast.LENGTH_SHORT).show();
        }
    }

//    public void createNewUser(FirebaseUser user,String name, long age) {
//        DatabaseReference mDatabase;
//        mDatabase = FirebaseDatabase.getInstance().getReference();
//
//        User newUser = new User(name, age, user.getEmail());
//        mDatabase.child("Users").child(user.getUid()).setValue(newUser);
//    }
    public String createNewCharity(String title, String description) {
        String charityId = mDatabaseRef.push().getKey();
        mDatabaseRef.child(charityId).child("title").setValue(title.trim());
        mDatabaseRef.child(charityId).child("description").setValue(description.trim());

        return charityId;
    }

}
