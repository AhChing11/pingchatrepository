package com.example.loginregisterproject;

public class Donation {
    long amount;
    String donatorName;
    String donatorId;

    public Donation() {

    }

    public Donation(String donatorId, String donatorName, long amount){
        this.donatorId = donatorId;
        this.donatorName = donatorName;
        this.amount = amount;
    }

    public long getAmount(){
        return amount;
    }
}
