package com.example.loginregisterproject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment {

    RecyclerView profileMenu;
    ImageView profileImage;
    String profileMenuText[];
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Users");
    FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    int profileMenuIcon[] = {R.drawable.edit_icon_foreground, R.drawable.back_icon_foreground};


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);

        profileMenu = view.findViewById(R.id.profileMenu);
        profileImage = view.findViewById(R.id.profileImage);

        profileMenuText = getResources().getStringArray(R.array.profileMenu);

        Query profilePicQuery = mDatabase.child(mFirebaseUser.getUid());
        profilePicQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("profilePicUrl")){
                    Picasso.get().load(dataSnapshot.child("profilePicUrl").getValue().toString()).into(profileImage);
//                    image.setImageBitmap(getBitmapFromURL(dataSnapshot.child("profilePicUrl").getValue().toString()));
//                    image.setImageResource(dataSnapshot.child("profilePicUrl").getValue());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        ProfileListAdapter profileListAdapter = new ProfileListAdapter(getContext(), profileMenuText, profileMenuIcon);
        profileMenu.setAdapter(profileListAdapter);
        profileMenu.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }


}
