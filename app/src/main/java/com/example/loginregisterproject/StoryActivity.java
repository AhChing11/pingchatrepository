//StoryActivity
package com.example.loginregisterproject;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.r0adkll.slidr.Slidr;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import jp.shts.android.storiesprogressview.StoriesProgressView;

public class StoryActivity extends AppCompatActivity implements StoriesProgressView.StoriesListener {

    int counter = 0;
    long pressTime = 0L;
    long limit = 500L;

    StoriesProgressView storiesProgressView;
    ImageView image, story_image;
    TextView story_username;
    TextView storyDuration;
    TextView descriptionBox;
    TextView moreHeader;
    View next;
    View previous;

    List<String> images;
    List<String> storyids;
    List<String> storyTime;
    List<String> description;
    String userid;

    private BottomSheetBehavior mBottomSheetBehavior;
    RelativeLayout bottomSheet;
//``````    RecyclerView moreList;``````



    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    System.out.println("----------------------------1x");
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    System.out.println("----------------------------2x");
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
                case MotionEvent.ACTION_CANCEL:
                    storiesProgressView.pause();
                    System.out.println("-----------------------------3x");
                    next.setOnTouchListener(onTouchListener);
                    previous.setOnTouchListener(onTouchListener);
                    break;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);

        storiesProgressView = findViewById(R.id.story_time);

        image = findViewById(R.id.image);
        story_image = findViewById(R.id.story_image);
        story_username = findViewById(R.id.story_username);
        userid = getIntent().getStringExtra("userid");
        storyDuration = findViewById(R.id.storyDuration);
        descriptionBox = findViewById(R.id.descriptionBox);
        moreHeader = findViewById(R.id.moreHeader);
        moreHeader.bringToFront();


        final View bottom_sheet = findViewById(R.id.bottom_sheet);
        bottomSheet = findViewById(R.id.bottomSheet);



        previous = findViewById(R.id.previous);
        next = findViewById(R.id.next);

        getStories(userid);
        userInfo(userid);

        final View parent = (View) moreHeader.getParent();  // button: the view you want to enlarge hit area
        parent.post( new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                moreHeader.getHitRect(rect);
                rect.top -= 0;    // increase top hit area
                rect.left -= 0;   // increase left hit area
                rect.bottom -= 900; // increase bottom hit area
                rect.right -= 900;  // increase right hit area
                parent.setTouchDelegate( new TouchDelegate( rect , moreHeader));
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    storiesProgressView.resume();

                } else if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
                    storiesProgressView.reverse();
                }
            }
        });
        previous.setOnTouchListener(onTouchListener);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    storiesProgressView.resume();

                } else if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
                    storiesProgressView.skip();
                }
            }
        });
        next.setOnTouchListener(onTouchListener);



//        moreList = findViewById(R.id.moreList);
//        setMoreList(moreList);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        mBottomSheetBehavior.setPeekHeight(190);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                    mBottomSheetBehavior.setPeekHeight(190);
                    storiesProgressView.resume();
                    moreHeader.setText("MORE");
                    System.out.println("-----------------------1");

                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    System.out.println("-----------------------2");
                    storiesProgressView.pause();
                    moreHeader.setText("Dismiss");
                } else if(newState== BottomSheetBehavior.STATE_SETTLING) {
                    bottomSheet.scrollTo(0,0);
                    System.out.println("-----------------------3");
                } else if(newState == BottomSheetBehavior.STATE_DRAGGING) {
                    System.out.println("-----------------------4");
                    storiesProgressView.pause();
                }
            }

            @Override
            public void onSlide(View bottomSheet, final float slideOffset) {

            }
        });

        moreHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    storiesProgressView.pause();
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    descriptionBox.setMovementMethod(new ScrollingMovementMethod());

                } else if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                }
            }
        });
//        moreHeader.setOnTouchListener(onTouchListener);


        Slidr.attach(this);




    }

    @Override
    public void onNext() {
        Glide.with(getApplicationContext()).load(images.get(++counter)).into(image);
        storyDuration.setText(storyTime.get(counter));
        descriptionBox.setText(description.get(counter));
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
        Glide.with(getApplicationContext()).load(images.get(--counter)).into(image);
        storyDuration.setText(storyTime.get(counter));
        descriptionBox.setText(description.get(counter));
    }


    @Override
    public void onComplete() {
        finish();
    }

    @Override
    protected void onDestroy() {
        storiesProgressView.destroy();
        super.onDestroy();
    }




    protected void onPause() {
        storiesProgressView.pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        storiesProgressView.resume();
        super.onResume();
    }

    private void getStories(String userid) {
        images = new ArrayList<>();
        storyids = new ArrayList<>();
        storyTime = new ArrayList<>();
        description = new ArrayList<>();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Story").child(userid);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                images.clear();
                storyids.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Story story = snapshot.getValue(Story.class);
                    long timecurrent = System.currentTimeMillis();
                    if (timecurrent > story.getTimestart() && timecurrent < story.getTimeend()) {
                        images.add(story.getImageurl().trim());
                        storyids.add(story.getStoryid().trim());
                        description.add(story.getStorydesc().trim());
                    }


                    int time = 0;
                    if ((timecurrent - story.getTimestart()) < 60000) {
                        time = (int) ((timecurrent - story.getTimestart()) / 1000);
                        storyDuration.setText(time + " secs ago");
                        storyTime.add(time + " secs ago");
                    } else if ((timecurrent - story.getTimestart()) < 3600000) {
                        time = (int) ((timecurrent - story.getTimestart()) / 60000);
                        storyDuration.setText(time + " mins ago");
                        storyTime.add(time + " mins ago");
                    } else if((timecurrent - story.getTimestart()) < 86400000) {
                        time = (int) ((timecurrent - story.getTimestart()) / 3600000);
                        storyDuration.setText(time + " hrs ago");
                        storyTime.add(time + " hrs ago");
                    } else {
                    }
                }
                descriptionBox.setText(description.get(0));
                storiesProgressView.setStoriesCount(images.size());
                storiesProgressView.setStoryDuration(5000L);
                storiesProgressView.setStoriesListener(StoryActivity.this);
                storiesProgressView.startStories(counter);

                Glide.with(getApplicationContext()).load(images.get(counter)).into(image);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void userInfo(String userid) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                story_username.setText(user.getUsername());
                Picasso.get().load(dataSnapshot.child("profilePicUrl").getValue().toString()).into(story_image);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

//    private void setMoreList(RecyclerView moreViewList) {
//        RecyclerView.Adapter moreAdapter;
//
//        ArrayList<MoreItem> moreArrayList = new ArrayList<>();
//        moreArrayList.add(new MoreItem("Donate", R.drawable.donation_icon1));
//        moreArrayList.add(new MoreItem("Add Charity", R.drawable.add_charity_icon));
//        moreArrayList.add(new MoreItem("Update Log", R.drawable.update_log_icon));
//        moreArrayList.add(new MoreItem("Notification", R.drawable.notification_icon));
//
//        moreViewList.setHasFixedSize(true);
//
//        moreAdapter = new moreAdapter(this, moreArrayList);
//
//        moreViewList.setLayoutManager(new GridLayoutManager(this, 3));
//        moreViewList.setAdapter(moreAdapter);
//    }


}
