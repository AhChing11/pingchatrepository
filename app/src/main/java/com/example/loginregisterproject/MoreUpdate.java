package com.example.loginregisterproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MoreUpdate extends AppCompatActivity {

    RecyclerView updateList;
    String updateTitle[];
    String updateDescription[];
    Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_update);

        updateList = findViewById(R.id.updateList);
        backButton = findViewById(R.id.backButton);

        updateTitle = getResources().getStringArray(R.array.updateTitle);
        updateDescription = getResources().getStringArray(R.array.updateDescription);

        final View parent = (View) backButton.getParent();  // button: the view you want to enlarge hit area
        parent.post( new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                backButton.getHitRect(rect);
                rect.top -= 100;    // increase top hit area
                rect.left -= 100;   // increase left hit area
                rect.bottom += 100; // increase bottom hit area
                rect.right += 100;  // increase right hit area
                parent.setTouchDelegate( new TouchDelegate( rect , backButton));
            }
        });

        UpdateListAdapter updateListAdapter = new UpdateListAdapter(this, updateTitle, updateDescription);
        updateList.setAdapter(updateListAdapter);
        updateList.setLayoutManager(new LinearLayoutManager(this));

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MoreUpdate.this, MainActivity.class));
            }
        });
    }
}
