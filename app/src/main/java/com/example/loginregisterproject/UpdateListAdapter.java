package com.example.loginregisterproject;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

public class UpdateListAdapter extends RecyclerView.Adapter<UpdateListAdapter.UpdateListViewHolder> {
    Context context;
    String updateTitleData[], updateDescriptionData[];
    private int positionSelected = -1;

    public UpdateListAdapter(Context ct, String updateTitle[], String updateDescription[]) {
        context = ct;
        updateTitleData = updateTitle;
        updateDescriptionData = updateDescription;
    }


    @NonNull
    @Override
    public UpdateListAdapter.UpdateListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.update_row, parent, false);
        return new UpdateListViewHolder(view);
    }


    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onBindViewHolder(@NonNull UpdateListAdapter.UpdateListViewHolder holder, int position) {

        //To get the initial hight before text is inserted(empty TextView).
        holder.updateDescription.measure(0,0);
        int initial_height = holder.updateDescription.getMeasuredHeight();

        holder.updateTitle.setText(updateTitleData[position]);
        holder.updateDescription.setText(updateDescriptionData[position]);


        if (position == positionSelected) {
            holder.updateDescription.setMaxLines(Integer.MAX_VALUE);
            holder.updateDescription.setEllipsize(null);

            holder.updateDescription.measure(0,0);
            int expanded_height = holder.updateDescription.getMeasuredHeight();

            //Expand the height of clicked item
            holder.updateItem.getLayoutParams().height = holder.updateItem.getLayoutParams().height + (expanded_height - initial_height);

        } else {
            holder.updateDescription.setMaxLines(1);
            holder.updateDescription.setEllipsize(TextUtils.TruncateAt.END);

            //Initialize the height to default height(before expand)
            holder.updateItem.getLayoutParams().height = 186;
        }
    }


    @Override
    public int getItemCount() {
        return updateTitleData.length;
    }

    public class UpdateListViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout updateItem;
        RelativeLayout updateRow;
        TextView updateTitle;
        TextView updateDescription;


        public UpdateListViewHolder(@NonNull View itemView) {
            super(itemView);

            updateItem = itemView.findViewById(R.id.updateItem);
            updateRow = itemView.findViewById(R.id.updateRow);
            updateTitle = itemView.findViewById(R.id.updateTitle);
            updateDescription = itemView.findViewById(R.id.updateDescription);

            updateRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() == positionSelected) {
                        // Unselect currently selected w/c means no selection.
                        positionSelected = -1;
                    } else {
                        positionSelected = getAdapterPosition();
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }
}
