package com.example.loginregisterproject;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.GridLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MoreFragment extends Fragment {

    private RecyclerView moreViewList;
    private RecyclerView.Adapter moreAdapter;
    private RecyclerView.LayoutManager layoutManager;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        ArrayList<MoreItem> moreArrayList = new ArrayList<>();
        moreArrayList.add(new MoreItem("Donate", R.drawable.donation_icon1));
        moreArrayList.add(new MoreItem("Add Charity", R.drawable.add_charity_icon));
        moreArrayList.add(new MoreItem("Update Log", R.drawable.update_log_icon));
        moreArrayList.add(new MoreItem("Notification", R.drawable.notification_icon));

        moreViewList = view.findViewById(R.id.moreList);
        moreViewList.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());
        moreAdapter = new moreAdapter(getContext(), moreArrayList);

        moreViewList.setLayoutManager(new GridLayoutManager(getContext(), 3));
        moreViewList.setAdapter(moreAdapter);

        return view;

    }
}
