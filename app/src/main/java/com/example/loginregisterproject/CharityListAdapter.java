package com.example.loginregisterproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.io.Resources;

public class CharityListAdapter extends RecyclerView.Adapter<CharityListAdapter.CharityListViewHolder> {
    String charityData[], descriptionData[];
    int images[];
    boolean checkState[];
    Context context;

    public CharityListAdapter(Context ct, String charity[], String description[], int image[], boolean checkState[]) {
        context = ct;
        charityData = charity;
        descriptionData= description;
        images = image;
        this.checkState = checkState;

    }

    @NonNull
    @Override
    public CharityListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.data_row, parent, false);
        return new CharityListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CharityListViewHolder holder,final int position) {

        holder.titleText.setText(charityData[position]);
        holder.descText.setText(descriptionData[position]);
        holder.charityImage.setImageResource(images[position]);


        if(checkState[position]) {
            holder.whiteBox.setBackgroundResource(R.drawable.bluebox);
            holder.descText.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.showMore.setTextColor(ContextCompat.getColor(context, R.color.white));
            DonateSelection.enableNextButton();
            checkState[position] = false;
            DonateSelection.setPosition(position);
        } else {
            holder.whiteBox.setBackgroundResource(R.drawable.box);
            holder.descText.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.showMore.setTextColor(ContextCompat.getColor(context, R.color.grey));
        }

        holder.charityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                if(checkState[position]) {
                    checkState[position]=false;
                } else {
                    checkState[position]=true;
                }
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class CharityListViewHolder extends RecyclerView.ViewHolder {
        TextView titleText, descText, showMore;
        ImageView charityImage;

        RelativeLayout whiteBox;
        RelativeLayout charityLayout;

        public CharityListViewHolder(@NonNull View itemView) {
            super(itemView);

            titleText = itemView.findViewById(R.id.titleText);
            descText = itemView.findViewById(R.id.descText);
            charityImage = itemView.findViewById(R.id.charityImage);
            whiteBox = itemView.findViewById(R.id.whiteBox);
            charityLayout = itemView.findViewById(R.id.charityLayout);
            showMore= itemView.findViewById(R.id.showMore);


        }

    }
}
