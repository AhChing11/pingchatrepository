package com.example.loginregisterproject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

public class ProfileListAdapter extends RecyclerView.Adapter<ProfileListAdapter.ProfileListViewHolder> {
    String profileMenuText[];
    int profileMenuIcon[];
    Context context;
    GoogleSignInClient mGoogleSignInClient;

    public ProfileListAdapter(Context context, String profileMenuText[], int profileMenuIcon[]) {
        this.profileMenuText = profileMenuText;
        this.profileMenuIcon = profileMenuIcon;
        this.context = context;
    }


    @NonNull
    @Override
    public ProfileListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.profile_row, parent, false);
        return new ProfileListViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull ProfileListViewHolder holder, final int position) {
        holder.profileMenuText.setText(profileMenuText[position]);
        holder.profileMenuIcon.setImageResource(profileMenuIcon[position]);

        holder.profileMenuRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    context.startActivity(new Intent(context, updateAcount.class));
                } else if (position == 1) {
                    FirebaseAuth.getInstance().signOut();
                    signOut();
                    context.startActivity(new Intent(context, Login.class));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return profileMenuIcon.length;
    }

    public class ProfileListViewHolder extends RecyclerView.ViewHolder {
        TextView profileMenuText;
        ImageView profileMenuIcon;
        RelativeLayout profileMenuBox;
        RelativeLayout profileMenuRow;

        public ProfileListViewHolder(@NonNull View itemView) {
            super(itemView);

            profileMenuText = itemView.findViewById(R.id.profileMenuText);
            profileMenuIcon = itemView.findViewById(R.id.profileMenuIcon);
            profileMenuBox = itemView.findViewById(R.id.profileMenuBox);
            profileMenuRow = itemView.findViewById(R.id.profileMenuRow);

        }
    }

    private void signOut() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);

        mGoogleSignInClient.signOut();
    }
}
