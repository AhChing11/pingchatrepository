package com.example.loginregisterproject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.TouchDelegate;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class updateAcount extends AppCompatActivity {
    Button updateButton;
    Button backButton;
    TextView updateNameInput;
    TextView updateAgeInput;
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Users");
    FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    RelativeLayout profilePicture;
    StorageReference storageReference;
    CircleImageView image;
    private StorageTask storageTask;
    private Uri mImageUri;
    String myUrl = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_acount);

        updateButton = findViewById(R.id.updateButton);
        updateNameInput = findViewById(R.id.updateNameInput);
        updateAgeInput = findViewById(R.id.updateAgeInput);
        profilePicture = findViewById(R.id.profilePicture);
        image = findViewById(R.id.image);
        backButton = findViewById(R.id.backButton);


        Query profilePicQuery = mDatabase.child(mFirebaseUser.getUid());
        profilePicQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("profilePicUrl")){
                    Picasso.get().load(dataSnapshot.child("profilePicUrl").getValue().toString()).into(image);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final View parent = (View) backButton.getParent();  // button: the view you want to enlarge hit area
        parent.post( new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                backButton.getHitRect(rect);
                rect.top -= 100;    // increase top hit area
                rect.left -= 100;   // increase left hit area
                rect.bottom += 100; // increase bottom hit area
                rect.right += 100;  // increase right hit area
                parent.setTouchDelegate( new TouchDelegate( rect , backButton));
            }
        });



        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(updateAcount.this, MainActivity.class));
            }
        });

        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storageReference = FirebaseStorage.getInstance().getReference("ProfilePicture");

                CropImage.activity().setAspectRatio(9, 16).start(updateAcount.this);
            }
        });

        Query displayQuery = mDatabase.child(mFirebaseUser.getUid());
        displayQuery.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name;
                String age;

                if(dataSnapshot.hasChild("name")){
                    name = dataSnapshot.child("name").getValue().toString();
                    updateNameInput.setText(name);
                }

                if(dataSnapshot.hasChild("age")) {
                    age = dataSnapshot.child("age").getValue().toString();
                    updateAgeInput.setText(age);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(updateAcount.this, "Update Cancelled.", Toast.LENGTH_SHORT).show();
            }


        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = updateNameInput.getText().toString();
                long age = Long.parseLong(updateAgeInput.getText().toString());

                mDatabase = mDatabase.child(mFirebaseUser.getUid());
                uploadProfilePhoto();
                mDatabase.child("name").setValue(name.trim());
                mDatabase.child("age").setValue(age);
                startActivity(new Intent(updateAcount.this, MainActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            mImageUri = result.getUri();
            image.setImageURI(mImageUri);
        } else {
            startActivity(new Intent(updateAcount.this, updateAcount.class));
            finish();
        }
    }

    private void uploadProfilePhoto() {

        if (mImageUri != null) {
            final StorageReference imageReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(mImageUri));

            storageTask = imageReference.putFile(mImageUri);
            storageTask.continueWithTask(new Continuation() {
                @Override
                public Task<Uri> then(@NonNull Task task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return imageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        myUrl = downloadUri.toString();

                        String myid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(myid);


                        reference.child("profilePicUrl").setValue(myUrl);

                        finish();
                    } else {
                        Toast.makeText(updateAcount.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(updateAcount.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "No image selected", Toast.LENGTH_SHORT).show();
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

}
