package com.example.loginregisterproject;

public class Charity {
    String title;
    String description;
    Upload upload;
    Donation donation;

    public Charity() {

    }

    public Charity(String title, String description) {
        this.title = title;
        this.description = description;
        upload = new Upload();
        donation = new Donation();
    }

    public void addDonation(String userId, String userName, long amount) {
        donation = new Donation(userId, userName, amount);
    }

}
